﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMS_InDeathMenu : GMS_GameManagerState
{
    public override void Enter()
    {
        base.Enter();

        m_target.m_inputManager.OnSimpleTouch += m_target.HNDL_SimpleClickOnDeathmenu;

        m_target.m_gameMenus.BH_SetScore((int)score);
        m_target.m_gameMenus.BH_SetDeathmenuStatus(true);   
    }

    public override void Exit()
    {
        m_target.m_inputManager.OnSimpleTouch -= m_target.HNDL_SimpleClickOnDeathmenu;

        m_target.m_gameMenus.BH_SetDeathmenuStatus(false);
    }

    public float score;
}
