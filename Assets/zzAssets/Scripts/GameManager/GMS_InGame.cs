﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMS_InGame : GMS_GameManagerState
{
    public override void Enter()
    {
        base.Enter();
        m_target.m_gameMenus.BH_SetDeathmenuStatus(false);
        m_target.m_gameMenus.BH_SetMainmenuStatus(false);
        m_target.m_gameMenus.BH_SetWonMenuStatus(false);

        m_target.m_playerBall.SM_GoToRunning();
        m_target.m_chaser.SM_GoToWaitingForChase();
        m_target.m_sonarPing.BH_SetEnabledStatus(true);

        m_target.m_playerBall.OnBallDies += m_target.HNDL_BallDies;
        m_target.m_playerBall.OnBallWin += m_target.HNDL_WinZoneReached;
    }


    public override void Exit()
    {
        base.Exit();

        m_target.m_sonarPing.BH_SetEnabledStatus(false);

        m_target.m_playerBall.OnBallDies -= m_target.HNDL_BallDies;
        m_target.m_playerBall.OnBallWin -= m_target.HNDL_WinZoneReached;
    }
}
