﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region event handler
    public void HNDL_BallDies(float distance) {
        SM_GoToInDeathMenu(distance);
    }

    public void HNDL_WinZoneReached (float distance)
    {
        SM_GoToInWonMenu(distance);
    }
    public void HNDL_SimpleClickOnDeathmenu()
    {
        //SM_GoToInMainMenu();
        SceneManager.LoadScene(0);
    }

    public void HNDL_SimpleClickInWonMenu()
    {
        if (SceneManager.GetActiveScene().buildIndex <= SceneManager.sceneCount)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        else
            SceneManager.LoadScene(0);
        
    }
    #endregion

    public void SM_GoToInMainMenu()
    {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_inMainMenu;
        m_states.m_current.Enter();
    }

    public void SM_GoToInGame() {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_inGame;
        m_states.m_current.Enter();
    }

    public void SM_GoToInDeathMenu(float score)
    {
        m_states.m_inDeathMenu.score = score;
            

        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_inDeathMenu;
        m_states.m_current.Enter();
    }

    public void SM_GoToInWonMenu (float score)
    {
        m_states.m_inWonMenu.score = score;

        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_inWonMenu;
        m_states.m_current.Enter();
    }
    private void Awake()
    {
        m_gameMenus = FindObjectOfType<GameMenus>();
        m_playerBall = FindObjectOfType<BallController>();
        m_chaser = FindObjectOfType<ChaserController>();
        m_sonarPing = FindObjectOfType<SonarPing>();

        m_inputManager = FindObjectOfType<InputManager_Base>();
        m_states.m_inMainMenu = (GMS_InMainMenu)new GMS_InMainMenu().Init(this, "InMainMenu");
        m_states.m_inGame = (GMS_InGame)new GMS_InGame().Init(this, "InGame");
        m_states.m_inDeathMenu = (GMS_InDeathMenu)new GMS_InDeathMenu().Init(this, "InDeathMenu");
        m_states.m_inWonMenu = (GMS_InWonMenu)new GMS_InWonMenu().Init(this, "InWonMenu");

        m_states.m_current = m_states.m_inMainMenu;
        m_states.m_current.Enter();
    }

    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {
        
    }

    [SerializeField]
    GameManagerStates m_states;
    [HideInInspector]
    public InputManager_Base m_inputManager;
    [HideInInspector]
    public BallController m_playerBall;
    [HideInInspector]
    public ChaserController m_chaser;
    [HideInInspector]
    public SonarPing m_sonarPing;
    public GameManagerReferences m_references;

    [HideInInspector]
    public GameMenus m_gameMenus;

    public string m_nextScene;


    [System.Serializable]
    public class GameManagerStates
    {
        public GMS_GameManagerState m_current;

        public GMS_Dying m_dying;
        public GMS_InDeathMenu m_inDeathMenu;
        public GMS_InGame m_inGame;
        public GMS_InMainMenu m_inMainMenu;
        public GMS_InWonMenu m_inWonMenu;
    }
    [System.Serializable]
    public class GameManagerReferences
    {
        
    }
}
