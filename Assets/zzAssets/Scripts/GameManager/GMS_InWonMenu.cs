﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMS_InWonMenu : GMS_GameManagerState
{
    public override void Enter()
    {
        base.Enter();
        m_target.m_inputManager.OnSimpleTouch += m_target.HNDL_SimpleClickInWonMenu;

        m_target.m_gameMenus.BH_SetScore((int)score);
        m_target.m_gameMenus.BH_SetWonMenuStatus(true);

    }


    public override void Exit()
    {
        base.Exit();
        m_target.m_inputManager.OnSimpleTouch -= m_target.HNDL_SimpleClickInWonMenu;

        m_target.m_gameMenus.BH_SetWonMenuStatus(false);
    }

    public float score;
}
