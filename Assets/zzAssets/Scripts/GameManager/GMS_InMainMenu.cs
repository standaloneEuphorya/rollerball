﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMS_InMainMenu : GMS_GameManagerState
{
    public override void Enter()
    {
        base.Enter();

        m_target.m_gameMenus.BH_SetDeathmenuStatus(false);
        m_target.m_gameMenus.BH_SetMainmenuStatus(true);

        m_target.m_inputManager.OnSimpleTouch += HNDL_SimpleClick;
    }

    public override void Exit()
    {
        base.Exit();
        m_target.m_inputManager.OnSimpleTouch -= HNDL_SimpleClick;
    }

    void HNDL_SimpleClick ()
    {
        m_target.SM_GoToInGame();
    }
}
