﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileAgent : MonoBehaviour
{
    public void Sleep()
    {
        gameObject.SetActive(false);
    }
    public void WakeUp(Transform spawnPoint)
    {
        gameObject.SetActive(true);
        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;
        m_rigidbody.velocity = transform.forward * m_velocity;
        m_backToPoolAt = Time.time + 4;
    }

    private void Update()
    {
        if (Time.time > m_backToPoolAt)
        {
            Sleep();
            m_ownerPool.ReturnMissile(this);
        }

    }

    private void Awake()
    {
        m_ownerPool = GetComponentInParent<MissilePoolManager>();
    }
    public float m_velocity = 10;
    public Rigidbody m_rigidbody;
    public float m_backToPoolAt; 
    public MissilePoolManager m_ownerPool;
}
