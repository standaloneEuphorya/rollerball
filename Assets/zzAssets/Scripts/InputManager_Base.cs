﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager_Base : MonoBehaviour
{

    private void Awake()
    {
        OnScreenTouched += DebugScreenTouched;
        OnScreenDoubleclicked += DebugScreenDoubleClicked;
        OnScreenUntouched += DebugScreenUntouched;
        OnSimpleTouch += DebugSimpleTouch;
    }


    public virtual void Update()
    {
        EvaluateHoldDown();
    }

    protected bool  EvaluateDoubleClick() {

        if (m_lastTouchTime + m_doubleClickTimer >= Time.time)
        {
            OnScreenDoubleclicked?.Invoke();
            m_lastTouchTime = Time.time;
            return true;
        }

        m_lastTouchTime = Time.time;
        return false;
    }

    protected void StartHoldDown ()
    {
        m_askedForHoldDown = true;
        m_holdDownStartTime = Time.time;
    }
    
    protected void EndHoldDown()
    {

        m_askedForHoldDown = false;
        if (!m_holdingDown)
        {
            OnSimpleTouch?.Invoke();
            return;
        }
        
        m_holdingDown = false;
        OnScreenUntouched?.Invoke();
    }
    protected void EvaluateHoldDown ()
    {
        if (!m_askedForHoldDown)
        {
            return;
        }

        if (Time.time >= m_holdDownStartTime + m_holdDownTimer)
        {
            OnScreenTouched?.Invoke();
            m_holdingDown = true;
            m_askedForHoldDown = false;
        }

    }
    
    void DebugSimpleTouch()
    {
       // Debug.Log("SCREEN SIMPLE TOUCH");
    }

    void DebugScreenTouched ()
    {

       // Debug.Log("SCREEN TOUCHED");
    }

    void DebugScreenUntouched()
    {

       // Debug.Log("SCREEN UNTOUCHED");
    }

    void DebugScreenDoubleClicked()
    {

        //Debug.Log("SCREEN DOUBLE CLICKED");
    }
    public event Action OnScreenTouched;
    public event Action OnScreenUntouched;
    public event Action OnScreenDoubleclicked;
    public event Action OnSimpleTouch;

    ////public void Call_OnScreenTouched()
    ////{
    ////    OnScreenTouched?.Invoke();
    ////}

    ////public void Call_OnScreenDoubleclicked()
    ////{
    ////    OnScreenDoubleclicked?.Invoke();
    ////}

    ////public void Call_OnScreenUntouched()
    ////{
    ////    OnScreenUntouched?.Invoke();
    ////}
    [SerializeField]
    protected float m_doubleClickTimer = .2f;
    [SerializeField]
    protected float m_holdDownTimer = .3f;

    [SerializeField]
    protected float m_lastTouchTime = -10;
    [SerializeField]
    protected float m_holdDownStartTime;
    [SerializeField]
    protected bool m_askedForHoldDown = false;
    [SerializeField]
    protected bool m_holdingDown = false;
}
