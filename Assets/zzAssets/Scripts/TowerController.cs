﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class TowerController : MonoBehaviour
{
    // Start is called before the first frame update
    public float m_range = 10;
    public Transform m_towerhead;
    public Transform m_cannonTop;
    public Transform m_target;
    public float m_shootDelay;
    public MissileAgent m_missile;
    public LookAtConstraint m_lookAtConstraint;
    float m_nextShotAt;
    public MissilePoolManager m_missilePool;

    private void Update()
    {
        m_towerhead.transform.LookAt(m_target.position);
        if (Vector3.Distance (m_target.position, m_towerhead.position) < m_range
            && Time.time >= m_nextShotAt)
        {
            m_nextShotAt = Time.time + m_shootDelay;
            //MissileAgent ma =  GameObject.Instantiate(m_missile);
            MissileAgent ma = m_missilePool.GetMissile();
            if (ma == null)
                return;
            ma.WakeUp(m_cannonTop);
        }
    }

    private void Awake()
    {


        m_target = FindObjectOfType<BallController>().transform;
        m_missilePool = FindObjectOfType<MissilePoolManager>();
        
        m_nextShotAt = Time.time + m_shootDelay;
    }
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(m_towerhead.position, m_range);
    }
}
