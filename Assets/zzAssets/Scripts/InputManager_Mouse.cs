﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager_Mouse : InputManager_Base
{

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (Input.GetMouseButtonDown(0))
        {
            if (EvaluateDoubleClick())
                return;

            StartHoldDown();                                 
        }

        if (Input.GetMouseButtonUp(0))
        {
            EndHoldDown();
        }

        
    }
}
