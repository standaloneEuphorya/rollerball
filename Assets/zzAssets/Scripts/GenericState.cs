﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GenericState <T> {

    public virtual void Enter()
    {
        Debug.Log("<color=cyan>STATUS CHANGE: </color>" + m_target.ToString()+"///"+m_target.GetType() + " ENTERS " + this.GetType().ToString());
    }
    public virtual void Exit() {

        Debug.Log("<color=cyan>STATUS CHANGE: </color>" + m_target.ToString() +" EXITS " + this.GetType().ToString());
    }
    public virtual void Update()
    {
        
    }

    public virtual void FixedUpdate()
    {

    }
    
    
    public GenericState<T> Init ( T target, string _name)
    {
        m_target = target;
        name = _name;
        return this;
    }
   protected T m_target;
    public string name; 
}
