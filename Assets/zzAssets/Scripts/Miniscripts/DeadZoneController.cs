﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZoneController : MonoBehaviour
{
    private void OnTriggerEnter(Collider col)
    {

        BallController other = col.gameObject.GetComponent<BallController>();
        if (other == null)
            return;

        other.SM_GoToDead();
    }

}
