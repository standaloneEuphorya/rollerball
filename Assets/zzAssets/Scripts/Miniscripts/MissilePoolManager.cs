﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissilePoolManager : MonoBehaviour
{
    public List <MissileAgent> m_pool;
    public void Start()
    {
        InitPool();
    }
    public void InitPool ()
    {
        foreach (MissileAgent item in m_pool)
        {
            item.Sleep();
        }
    }

    public void ReturnMissile (MissileAgent missile)
    {
        m_pool.Add(missile);
    }
    public MissileAgent GetMissile ()
    {
        if (m_pool.Count == 0)
            return null;

        MissileAgent retrievedAgent = m_pool[0];
        m_pool.RemoveAt(0);
        return retrievedAgent;

    }
}
