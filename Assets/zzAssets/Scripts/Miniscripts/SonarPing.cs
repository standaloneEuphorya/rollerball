﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarPing : MonoBehaviour
{

    public void BH_SetEnabledStatus (bool status)
    {

        enabled = status;
        m_audioSource.enabled = status;

    }


    private void Update()
    {
        if (Time.time >= m_nextPingTime)
        {
            m_audioSource.Play();
            float distance = Vector3.Distance(transform.position, m_chaser.transform.position);
            //distance = Mathf.Clamp(distance, m_minPingDistance, m_maxPingDistance);

            m_nextPingTime = Time.time + Mathf.Lerp(m_minPingDelay, m_maxPingDelay, distance  / m_maxPingDistance);
            //Debug.Log("-"+distance+"--> " + (m_nextPingTime - Time.time).ToString());
        }



    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (m_chaser != null)
            Gizmos.DrawLine(transform.position, m_chaser.transform.position);
    }
    private void Awake()
    {
        m_chaser = FindObjectOfType<ChaserController>();
    }

    private void Start()
    {
        BH_SetEnabledStatus(false);
    }
    [SerializeField]
    AudioClip m_pingAudio;
    [SerializeField]
    AudioSource m_audioSource;

    [SerializeField]
    float m_maxPingDelay = 5;
    [SerializeField]
    float m_maxPingDistance = 10;
    [SerializeField]
    float m_minPingDistance = 1.5f;
    [SerializeField]
    float m_minPingDelay = 1.2f;

    ChaserController m_chaser;

    public bool m_pingLaunched = false;
    public bool m_availableForPing = true;
   public  bool m_pingAsked = false;
   public    float m_nextPingTime;

    public float m_releasePingAt;
}
