﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenus : MonoBehaviour
{
    public void BH_SetMainmenuStatus (bool targetStatus)
    {
        m_mainMenu.SetActive(targetStatus);
    }

    public void BH_SetDeathmenuStatus (bool targetStatus)
    {
        m_deathMenu.SetActive(targetStatus);
    }

    public void BH_SetWonMenuStatus (bool targetStatus)
    {
        m_wonMenu.SetActive(targetStatus);
    }
    public void BH_SetScore (int score)
    {
        m_txtScoreDeath.text = score.ToString();
        m_txtWonMenu.text = (score).ToString();
    }

    private void Awake()
    {
       //( BH_SetMainmenuStatus(false);
        BH_SetDeathmenuStatus(false);
        BH_SetWonMenuStatus(false);
    }

    [SerializeField]
    GameObject m_mainMenu;
    [SerializeField]
    GameObject m_deathMenu;
    [SerializeField]
    Text m_txtScoreDeath;
    [SerializeField]
    GameObject m_wonMenu;
    [SerializeField]
    Text m_txtWonMenu;
}
