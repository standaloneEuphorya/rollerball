﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinZoneController : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        BallController other = collision.gameObject.GetComponent<BallController>();
        if (other == null)
            return;

        other.SM_GoToMatchWon();
    }
}
