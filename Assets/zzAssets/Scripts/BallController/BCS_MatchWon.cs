﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCS_MatchWon : BCS_BallControllerState
{
    public override void Enter()
    {
        base.Enter();
        m_target.CallOnBallWin(score + score*.5f);
        m_target.BH_SetSubcomponentsStatus(false);
    }


    public float score;
    
}
