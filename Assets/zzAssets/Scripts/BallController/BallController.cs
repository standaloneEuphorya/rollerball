﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    #region state management
    public void SM_GoToIdle()
    {   
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_idle;
        //m_states.m_currentName = m_states.m_current.name;
        m_states.m_current.Enter();
    }

    public void SM_GoToDisabled()
    {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_disabled;
        m_states.m_current.Enter();
    }
    public void SM_GoToRunning()
    {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_running;
//        m_states.m_currentName = m_states.m_current.name;
        m_states.m_current.Enter();
    }

    public void SM_GoToDead ()
    {
        m_states.m_dead.distance = m_score;

        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_dead;
        m_states.m_current.Enter();
    }

    public void SM_GoToMatchWon()
    {
        m_states.m_matchWon.score = m_score;

        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_matchWon;
        m_states.m_current.Enter();
    }
    #endregion

    public void BH_SetSubcomponentsStatus(bool isPlaying)
    {
        m_references.m_rigidbody.isKinematic = !isPlaying;
        m_references.m_meshRenderer.enabled = isPlaying;
        
    }

    #region to be called from external entites
    public Vector3 GetVelocity ()
    {
        return m_references.m_rigidbody.velocity;
    }
    #endregion

    private void Update()
    {
        m_states.m_current?.Update();
    }
    private void Awake()
    {
        m_states.m_running = (BCS_Running)new BCS_Running().Init(this, "Running");
        m_states.m_idle = (BCS_Idle)new BCS_Idle().Init(this, "Idle");
        m_states.m_disabled = (BCS_Disabled)new BCS_Disabled().Init(this, "Disabled");
        m_states.m_dead = (BCS_Dead)new BCS_Dead().Init(this, "Dead");
        m_states.m_matchWon = (BCS_MatchWon)new BCS_MatchWon().Init(this, "MatchWon");

        p_initialDistance = Vector3.Distance(m_references.m_levelTarget.transform.position, transform.position);

        m_spawningPoint = transform.position;

        m_states.m_current = m_states.m_disabled;
        m_states.m_current.Enter();
    }


    #region event callers
    public void CallOnBallPushed (Vector3 direction)
    {
        OnBallPushed?.Invoke(direction);
    }
    public void CallOnBallStopPushing ()
    {
        OnBallStopPushing?.Invoke();
    }
    public void CallOnBallControlStarts ()
    {
        OnBallControlStarts?.Invoke();
    }
    public void CallOnBallDies (float distance)
    {
        OnBallDies?.Invoke(distance);
    }
    public void CallOnBallWin(float distance)
    {
        OnBallWin?.Invoke(distance);
    }
    public void CallOnBallSpawns ()
    {
        OnBallSpawns?.Invoke();
    }
    #endregion

    public delegate void Vector3ValueDelegate(Vector3 value);
    public delegate void RenameValueDelegate(float value);

    public event Vector3ValueDelegate OnBallPushed;
    public event Action OnBallStopPushing;
    public event Action OnBallControlStarts;
    public event RenameValueDelegate OnBallDies;
    public event RenameValueDelegate OnBallWin;
    public event Action OnBallSpawns;

    [SerializeField]
    public BallControllerStates m_states;

    public float m_score;

    [Header("Settings")]
    public LayerMask m_canRunOver;
    public float m_forcePerSecond = 1;
    public float m_jumpingForce = 5;
    public float m_maxSpeed = 10;
    [SerializeField]
    public BallControllerReferences m_references;

    public float p_initialDistance { get; set; }
    public float p_currentDistance { get; set; }

    [HideInInspector]
    public Vector3 m_spawningPoint;

    [System.Serializable]
    public class BallControllerReferences
    {
        public Transform m_camera;
        public InputManager_Base m_inputManager;
        public Rigidbody m_rigidbody;
        public MeshRenderer m_meshRenderer;
        public TriangleExplosion m_triangleExplosion;
        public AudioSource m_mainAudioSource;
        public Transform m_levelTarget;
    }

    [System.Serializable]
    public class BallControllerStates
    {
     //   public string m_currentName;

        public BCS_BallControllerState m_current;
        public BCS_Running m_running;        
        public BCS_Idle m_idle;
        public BCS_Disabled m_disabled;
        public BCS_Dead m_dead;
        public BCS_MatchWon m_matchWon;
    }
}
