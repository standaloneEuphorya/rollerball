﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCS_Dead : BCS_BallControllerState
{
    public override void Enter()
    {
        base.Enter();
        m_target.CallOnBallDies(distance);

        m_target.BH_SetSubcomponentsStatus(false);
        m_target.m_references.m_triangleExplosion.BH_CallForExplosionFX();
        m_target.m_references.m_mainAudioSource.Play();
    }

    public float distance;
}
