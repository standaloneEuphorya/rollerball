﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallFollower : MonoBehaviour
{
    [SerializeField]
    Transform m_target;

    [SerializeField]
    Vector3 m_offset;
    private void Start()
    {
        m_offset = m_target.InverseTransformPoint(transform.position);
    }
    private void Update()
    {
        transform.position = m_target.transform.position + m_offset;
    }
}
