﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BCS_Running : BCS_BallControllerState
{
    public override void Enter()
    {
        base.Enter();
        m_pushingBall = false;
        m_target.m_score = 0;
        m_previousPosition = m_target.transform.position;
        m_target.m_references.m_inputManager.OnScreenTouched += BH_StartPushingBall;
        m_target.m_references.m_inputManager.OnScreenUntouched += BH_StopPushingBall;
        m_target.m_references.m_inputManager.OnScreenDoubleclicked += BH_JumpBall;

        m_target.transform.position = m_target.m_spawningPoint;
        m_target.CallOnBallSpawns();

        m_target.BH_SetSubcomponentsStatus(true);
    }

    public override void Update()
    {
        base.Update();
        m_isOnFloor = IsOnFloor();

        if (m_pushingBall)
        {
            BH_PushBall();
            //Debug.Log("Ball speed " + m_target.m_references.m_rigidbody.velocity.magnitude);
        }

        if (m_isOnFloor)
        {
            m_target.m_score += Vector3.Distance(m_previousPosition, m_target.transform.position);
            m_previousPosition = m_target.transform.position;
        }

        m_target.p_currentDistance = Vector3.Distance(m_target.m_references.m_levelTarget.position, m_target.transform.position);

    }
    public override void Exit()
    {
        base.Exit();

        m_target.m_references.m_inputManager.OnScreenTouched -= BH_StartPushingBall;
        m_target.m_references.m_inputManager.OnScreenUntouched -= BH_StopPushingBall;
        m_target.m_references.m_inputManager.OnScreenDoubleclicked -= BH_JumpBall;
    }

    public void BH_PushBall ()
    {
        if (!m_isOnFloor || m_target.m_references.m_rigidbody.velocity.magnitude >= m_target.m_maxSpeed)
            return;
        
        RaycastHit hit;
        if (Physics.Raycast(m_target.m_references.m_camera.transform.position, m_target.m_references.m_camera.transform.forward, out hit, Mathf.Infinity, m_target.m_canRunOver)) 
        {


            Vector3 targetPoint = Vector3.ProjectOnPlane(hit.point - m_target.transform.position, Vector3.up);
            targetPoint += m_target.transform.position;

            ////GameObject dummy = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            ////dummy.transform.position = targetPoint; 


          

            Vector3 forceDirection = (targetPoint - m_target.transform.position).normalized;
            m_target.m_references.m_rigidbody.AddForce(forceDirection * Time.deltaTime * m_target.m_forcePerSecond);
            m_target.CallOnBallPushed(forceDirection + m_target.transform.position);
        } 
    }

    public void BH_JumpBall ()
    {
        if (!m_isOnFloor)
            return;
        m_target.m_references.m_rigidbody.AddForce(m_target.m_jumpingForce * (m_target.m_references.m_rigidbody.velocity.normalized + Vector3.up*2).normalized , ForceMode.Impulse);
    }


    public void BH_StartPushingBall()
    {
        m_pushingBall = true;
    }

    public void BH_StopPushingBall ()
    {
        m_pushingBall = false;
        m_target.CallOnBallStopPushing();
    }

    public bool IsOnFloor ()
    {
        Vector3 origin = m_target.transform.position;
        Debug.DrawRay(origin, -Vector3.up /1.8f, Color.cyan);

        RaycastHit hit;
        if (Physics.Raycast(origin, -Vector3.up, out hit, 1 / 1.8f, m_target.m_canRunOver))
        {
            return true;
        }
        else
            return false;
        
    }


    bool m_isOnFloor;
    bool m_pushingBall = false;
    Vector3 m_previousPosition;
    

}
