﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCS_Disabled : BCS_BallControllerState
{
    public override void Enter()
    {
        base.Enter();
        m_target.gameObject.SetActive(false);
    }

    public override void Exit()
    {
        base.Exit();
        m_target.gameObject.SetActive(true);
    }
}
