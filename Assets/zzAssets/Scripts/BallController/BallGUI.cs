﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallGUI : MonoBehaviour
{
    public void HNDL_OnBallDies (float value)
    {
        HNDL_BallIsUnpushed();
        m_mainCanvas.enabled = false;
    }

    public void HNDL_OnBallSpawns ()
    {
        m_mainCanvas.enabled = true;
    }
    void HNDL_BallIsPushed (Vector3 lookAtPosition)
    {
        m_forceDirectionGauge.gameObject.SetActive(true);
        m_forceDirectionGauge.transform.LookAt(lookAtPosition);
    }

    void HNDL_BallIsUnpushed()
    {
        m_forceDirectionGauge.gameObject.SetActive(false);
    }

    private void Update()
    {
        Vector3 horizontalVelocity = m_ballController.GetVelocity();
        horizontalVelocity.y = 0;

        m_txtSpeed.text = (horizontalVelocity.magnitude * 10).ToString("F0");

        m_mainSlider.value = m_ballController.p_initialDistance - m_ballController.p_currentDistance;
    }

    private void Awake()
    {
        BH_ResetStatus();
    }
    private void Start()
    {
        HNDL_OnBallDies(0);

        m_mainSlider.maxValue = m_ballController.p_initialDistance;
    }
    void BH_ResetStatus ()
    {
        m_forceDirectionGauge.gameObject.SetActive(false);
    }


    private void OnEnable()
    {
        m_ballController.OnBallPushed += HNDL_BallIsPushed;
        m_ballController.OnBallStopPushing += HNDL_BallIsUnpushed;
        

        m_ballController.OnBallDies += HNDL_OnBallDies;
        m_ballController.OnBallWin += HNDL_OnBallDies;
        m_ballController.OnBallSpawns += HNDL_OnBallSpawns;
    }

    private void OnDisable()
    {
        m_ballController.OnBallPushed -= HNDL_BallIsPushed;
        m_ballController.OnBallStopPushing -= HNDL_BallIsUnpushed;

        m_ballController.OnBallDies -= HNDL_OnBallDies;
        m_ballController.OnBallWin -= HNDL_OnBallDies;
        m_ballController.OnBallSpawns -= HNDL_OnBallSpawns;
    }



    [SerializeField]
    BallController m_ballController;
    [SerializeField]
    Transform m_forceDirectionGauge;
    [SerializeField]
    Canvas m_mainCanvas;
    [SerializeField]
    Text m_txtSpeed;
    [SerializeField]
    Slider m_mainSlider;




}
