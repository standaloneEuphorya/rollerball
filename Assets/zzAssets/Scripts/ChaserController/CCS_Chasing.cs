﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCS_Chasing : CCS_ChaserControllerState
{
    public override void Enter()
    {
        base.Enter();
        m_target.m_target.OnBallDies += m_target.HNDL_OnBallDies;
        m_target.m_target.OnBallWin += m_target.HNDL_OnBallDies;
        m_target.m_mainAudioSource.Play();


    }

    public override void Update()
    {
        base.Update();
        m_target.BH_RefreshWaypoint(false);
        MoveTowardsTarget();
        TryToKillTarget();
    }

    void MoveTowardsTarget ()
    {
        
        if (m_target.m_waypoints.Count == 0) {

            m_target.transform.position = Vector3.MoveTowards(m_target.transform.position, m_target.m_target.transform.position, Time.deltaTime* m_target.m_chasingSpeed);
        }
        else
        {
            m_target.transform.position = Vector3.MoveTowards(m_target.transform.position, m_target.m_waypoints[0], Time.deltaTime * m_target.m_chasingSpeed);
            if (Vector3.Distance(m_target.transform.position, m_target.m_waypoints[0]) == 0)
                m_target.m_waypoints.RemoveAt(0);
        }

    }


    public override void Exit()
    {
        base.Exit();
        m_target.m_target.OnBallDies -= m_target.HNDL_OnBallDies;

        m_target.m_target.OnBallWin -= m_target.HNDL_OnBallDies;

        m_target.m_mainAudioSource.Stop();
    }


    public  bool TryToKillTarget ()
    {
        Collider[] hitColliders = Physics.OverlapSphere(m_target.transform.position, m_target.transform.localScale.x / 2, m_target.m_targetLayers);
        foreach (Collider item in hitColliders)
        {
            BallController hitBallController = item.GetComponent<BallController>();
            if (hitBallController != null)
            {
                hitBallController.SM_GoToDead();
            }
        }
        return false;
    }
}
