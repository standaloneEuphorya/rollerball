﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCS_WaitingForChase : CCS_ChaserControllerState
{
    public override void Enter()
    {
        base.Enter();
        m_target.transform.position = m_target.m_spawningPoint;
        m_target.m_waypoints.Clear();
        m_target.BH_RefreshWaypoint(true);
        m_startChasingTime = Time.time + m_target.m_startChasingDelay;


        m_target.m_target.OnBallDies += m_target.HNDL_OnBallDies;
        m_target.m_target.OnBallWin += m_target.HNDL_OnBallDies;
    }

    public override void Update()
    {
        base.Update();
        m_target.BH_RefreshWaypoint(false);

        if (Time.time >= m_startChasingTime)
            m_target.SM_GoToChasing();
    }

    public override void Exit()
    {
        base.Exit();

        m_target.m_target.OnBallDies -= m_target.HNDL_OnBallDies;
        m_target.m_target.OnBallWin -= m_target.HNDL_OnBallDies;
    }


    float m_startChasingTime;



}
