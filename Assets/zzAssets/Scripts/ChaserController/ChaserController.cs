﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaserController : MonoBehaviour
{

    #region state management
    public void SM_GoToDisabled ()
    {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_disabled;
        m_states.m_current.Enter();
    }

    public void SM_GoToWaitingForChase ()
    {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_waitingForChase;
        m_states.m_current.Enter();
    }

    public void SM_GoToChasing()
    {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_chasing;
        m_states.m_current.Enter();
     }

    public void SM_GoToTargetReached ()
    {
        m_states.m_current?.Exit();
        m_states.m_current = m_states.m_targetReached;
        m_states.m_current.Enter();
    }
    #endregion
    #region event handlers
    public void HNDL_OnBallDies(float score)
    {
        SM_GoToTargetReached();
    }

    #endregion
    public void BH_RefreshWaypoint (bool forceRefresh)
    {
        ////if (m_waypoints.Count>0)
        ////    Debug.Log("----> " + Vector3.Distance(m_waypoints[m_waypoints.Count - 1], m_target.transform.position));


        if (forceRefresh)
        {
            m_waypoints.Add(m_target.transform.position);
            m_nextNewWaypointTime = Time.time + m_waypointRefreshTime;
            return;
        }

        if (Time.time > m_nextNewWaypointTime 
         || forceRefresh)
        {
                
            if (m_waypoints.Count == 0
              ||(Vector3.Distance(m_waypoints[m_waypoints.Count - 1], m_target.transform.position) >= m_minimumWaypointDistance))
            {           
                m_waypoints.Add(m_target.transform.position);
                m_nextNewWaypointTime = Time.time + m_waypointRefreshTime;
            }
        }
    }

    private void Update()
    {
        m_states.m_current.Update();
    }


    private void Awake()
    {
        m_waypoints = new List<Vector3>();

        m_spawningPoint = transform.position;

        m_states.m_disabled = (CCS_Disabled) new CCS_Disabled().Init(this, "Disabled");
        m_states.m_waitingForChase = (CCS_WaitingForChase)new CCS_WaitingForChase().Init(this, "WaitingForChase");
        m_states.m_chasing = (CCS_Chasing)new CCS_Chasing().Init(this, "Chasing");
        m_states.m_targetReached = (CCS_TargetReached)new CCS_TargetReached().Init(this, "TargetReached");

        m_states.m_current = m_states.m_disabled;
        m_states.m_current.Enter();
    }


    private void OnDrawGizmos ()
    {
        Gizmos.color = Color.red;

        //foreach (Vector3 item in m_waypoints) {
       for (int i = 0; i < m_waypoints.Count; i++) { 
            Gizmos.DrawWireSphere(m_waypoints[i], .5f);

            if (i < m_waypoints.Count - 1)
            {
                Gizmos.DrawLine(m_waypoints[i], m_waypoints[i + 1]);
            }
        }
    }
    [SerializeField]
    ChaserControllerStates m_states;
    [SerializeField]
    public List<Vector3> m_waypoints;

    [Header("Settings")]
    public LayerMask m_targetLayers;
    [SerializeField]
    float m_waypointRefreshTime =1;
    float m_minimumWaypointDistance = 3;
    float m_nextNewWaypointTime = 0;    
    public float m_startChasingDelay =15;
    public float m_chasingSpeed;

    [SerializeField]
    public BallController m_target;
    [SerializeField]
    public AudioSource m_mainAudioSource;

    [HideInInspector]
    public Vector3 m_spawningPoint;

    
    [System.Serializable]
    public class ChaserControllerStates
    {
        public CCS_ChaserControllerState m_current;
        public CCS_Disabled m_disabled;
        public CCS_WaitingForChase m_waitingForChase;
        public CCS_Chasing m_chasing;
        public CCS_TargetReached m_targetReached;
    }
}
